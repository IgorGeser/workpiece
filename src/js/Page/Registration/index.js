import React, {Component} from 'react'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import InputMask from 'react-input-mask';
import { withRouter } from 'react-router-dom';
import * as AuthActions from '../../REDUX/ducks/auth'
import './style.scss';

class Registration extends Component {
    /*
    * checked user name input and validate by only [a-z A-Z]
    * */
    handlerCheckFullName(e) {
        const value = e.target.value.trim();
        const strRegex = /([a-z A-Z])/g.source;
        const {validate_name} = this.props.AuthActions;
        const domRef = e.target;
        
        this.validate(strRegex, value, validate_name, domRef)
    }
    
    /*
    * checked email input and validate by standard email type
    * */
    handlerCheckEmail(e) {
        const value = e.target.value
        const strRegex = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/g.source;
        const {validate_email} = this.props.AuthActions;
        const domRef = e.target;
        
        this.validate(strRegex, value, validate_email, domRef);
    };
    
    /*
    * checked phone input and validate by phone type
    * */
    handlerCheckTelephone (e) {
        const value = e.target.value.split(' ').join('');
        const strRegex = /^([+0-9]{13})$/g.source;
        const {validate_phone} = this.props.AuthActions;
        const domRef = e.target;
        
        this.validate(strRegex, value, validate_phone, domRef);
    };
    
    /*
    * check password input and compare it with rePassword
    * call func: handlerCheckRePassword
    * */
    handlerCheckPassword (e) {
        const value = e.target.value;
        const strRegex = /^\w{8,16}$/g.source;
        const {validate_password} = this.props.AuthActions;
        const domRef = e.target;
        
        this.handlerCheckRePassword();
        this.validate(strRegex, value, validate_password, domRef);
    };
    
    /*
    * check if input rePassword coincides with password
    * */
    handlerCheckRePassword () {
        const {check_password} = this.props.AuthActions;
    
        this.validateRePassword(check_password) ;
    };
    
    /*
    * validate input and if this passed regex rules set style .passed
    * else set .failed
    * */
    validate (strRegex, testingStr, action, el) {
        const regex = new RegExp(strRegex, 'g');
        const passed = 'Registration__passed';
        const failed = 'Registration__failed';
        
        if (testingStr.length === 0) {
            el.classList.remove(failed, passed);
        
            return;
        }
        
        if (regex.test(testingStr)) {
            el.classList.remove(failed);
            el.classList.add(passed);
            action(true)
        } else {
            el.classList.remove(passed);
            el.classList.add(failed);
            action(false)
        }
    };
    
    /*
    * check if password === rePassword
    * */
    validateRePassword(action) {
        const passed = 'Registration__passed';
        const failed = 'Registration__failed';
        const el = this.refs.rePassword;

        if (this.refs.password.value === el.value &&
               (this.refs.password.value.length !== 0 ||
                 this.refs.password.value.length > 8)
           ) {
            el.classList.remove(failed);
            el.classList.add(passed);
            action(true)
        } else {
            el.classList.remove(passed);
            el.classList.add(failed);
            action(false)
        }
    }
    
    /*
    * if all is ok authorization user
    * */
    regUser () {
        const validation = this.checkValidationObject();
        
        if (validation) {
            console.log('authorization!')
        } else {
            console.error('authentication error!')
        }
    };
    
    /*
    * check all fields of validation
    * */
    checkValidationObject () {
        const {validateName, validateEmail, validateTelephone,
            validatePassword, checkPassword} = this.props.auth;
        
        const {is_validate} = this.props.AuthActions;
        
        if (validateName && validateEmail &&
            validateTelephone && validatePassword && checkPassword) {
            is_validate(true);
            return true
        } else {
            is_validate(false);
            return false
        }
        
    };
    
    /*
    * push error if validate is false
    * */
    errorMessage(isValidation, errorString) {
        const error = <p className="Registration__failed-label">{errorString}</p>;
        
        return isValidation === true || isValidation === 'empty' ? '' : error;
    }
    
    
    render() {
        const {validateName, validateEmail, validateTelephone,
            validatePassword, checkPassword} = this.props.auth;
        
        return (
            <div className="Registration__wrapper">
                <div className="Registration__header-blue">
                    <header>
                        <div className="logo">
                            <img src="./../../assets/img/logo.svg" alt="logo"/>
                        </div>
                    </header>
                </div>
                
                <div className="Registration__sub-wrapper">
                    <p className="Registration__sub-title">
                        Реєстрація користувача
                    </p>
                    <div className="Registration__root">
                        <div className="Registration__container">
                            <div className="Registration__formGroup">
                                <input
                                    type="text"
                                    className="Registration__inputReg"
                                    placeholder="Ім'я користувача"
                                    ref="fullName"
                                    required
                                    autoFocus // eslint-disable-line jsx-a11y/no-autofocus
                                    onBlur={::this.handlerCheckFullName}
                                />
                                {this.errorMessage(validateName, 'Невідповідний формат даних')}
                            </div>
                            <div className="Registration__formGroup">
                                <input
                                    className="Registration__inputReg"
                                    type="text"
                                    ref="email"
                                    placeholder="E-mail"
                                    required
                                    onBlur={::this.handlerCheckEmail}
                                />
                                {this.errorMessage(validateEmail, 'Невідповідний формат даних')}
                            </div>
                            <div className="Registration__formGroup">
                                <InputMask
                                    className="Registration__inputReg telephone telephoneLabel"
                                    type="tel"
                                    ref="phone"
                                    required
                                    placeholder="Телефон"
                                    mask="+38 999 999 99 99"
                                    maskChar=" "
                                    onBlur={::this.handlerCheckTelephone}
                                />
                                {this.errorMessage(validateTelephone, 'Невідповідний формат даних')}
                            </div>
                            <div className="Registration__formGroup">
                                <input
                                    ref="password"
                                    className="Registration__inputReg"
                                    type="password"
                                    required
                                    placeholder="Пароль"
                                    maxLength={16}
                                    onBlur={::this.handlerCheckPassword}
                                />
                                {this.errorMessage(validatePassword, 'Невідповідний формат даних')}
                            </div>
                            <div className="Registration__formGroup">
                                <input
                                    ref="rePassword"
                                    className="Registration__inputReg"
                                    type="password"
                                    required
                                    placeholder="Підтвердити пароль"
                                    maxLength={16}
                                    onBlur={::this.handlerCheckRePassword}
                                />
                                {this.errorMessage(checkPassword, 'Паролі не співпадають')}
                            </div>
                            <div className="Registration__formGroup">
                                <button className="Registration__button" type="submit"
                                        onClick={::this.regUser}>
                                    Зареєструватися
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps( state ) {
    return {
        auth: state.auth,
    }
}

function mapDispatchToProps( dispatch ) {
    return {
        AuthActions: bindActionCreators(AuthActions, dispatch),
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Registration));