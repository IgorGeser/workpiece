import React, {Component} from 'react'
import {withRouter, Link} from 'react-router-dom'
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as AuthActions from '../../REDUX/ducks/auth'

import './style.scss'


class Login extends Component {
    
    constructor(props) {
        super(props);
        
        this.validatorEmail = this.validatorEmail.bind(this)
        this.handlePassword = this.handlePassword.bind(this)
        this.enter = this.enter.bind(this)
        this.closeModal = this.closeModal.bind(this)
    }
    
    /*
     *  password handler
     *  if we wanna validate password field, we can do it here
     */
    handlePassword(e) {
        console.log(this)
        let passwordInput = e.target.value
        // const {add_password} = this.props.LoginActions
        const { validate_password } = this.props.AuthActions

        /*
         in future development!
         */
        if (passwordInput.length > 8 && passwordInput.length < 17)
            validate_password(true)
        else
            validate_password(false)
    }
    
    /*
     Validates input with email
     --------------------------
     if true
     input take green color
     else
     input take red color
     
     */
    validatorEmail(e) {
        let BuffEmail = e.target.value
        let emailDOM = e.target
        let re = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/
        let found = re.test(BuffEmail)
        
        const { validate_email } = this.props.AuthActions
        if (BuffEmail.length) {
            if (found && emailDOM) {
                emailDOM.classList.remove('Login__failed')
                emailDOM.classList.add('Login__passed')
                validate_email(true)
            } else if (emailDOM) {
                emailDOM.classList.remove('Login__passed')
                emailDOM.classList.add('Login__failed')
                validate_email(false)
            }
        } else if (BuffEmail.length === 0) {
            emailDOM.classList.remove('Login__failed')
            emailDOM.classList.remove('Login__passed')
        }
    }
    
    /*
     *  entry function  check all login function (take redux value)
     *  if (all good)
     *      --> Authorization
     *  else
     *      --> throw error
     */
    enter() {
        let email = this.props.auth.validateEmail
        let password = this.props.auth.validatePassword

        console.log(email, ' ', password)
        const { modal_view } = this.props.AuthActions

        if (email && password){
            console.log('well done!')
            this.refs.email.classList.remove('Login__failed')
            this.refs.password.classList.remove('Login__failed')
            this.refs.email.classList.add('Login__passed')
            this.refs.email.classList.add('Login__passed')
        }

        else if (email === false || password === false) {
            this.refs.modal.classList.remove('Login__modal__close')
            this.refs.modal.classList.add('Login__modal__open')

            modal_view(true)
        }

        if (email === false) {
            this.refs.email.classList.add('Login__failed')
        }
        if (password === false) {
            this.refs.password.classList.add('Login__failed')
        }
    }

    /*
    *   click 'x' ----> close modal window
    * */

    closeModal() {
        let view = this.props.view

        if (!view) {
            this.refs.modal.classList.remove('Login__modal__open')
            this.refs.modal.classList.add('Login__modal__close')
        }
    }



    render() {
        return (
            <div className="Login__box">
                <div className="Login__main">
                    <img src="./../../assets/img/logo.svg" alt="logo"/>
                    
                    <p>Авторизація</p>
                    <input type="text"
                           className="Login__input"
                           placeholder="E-mail"
                           ref = "email"
                           onBlur={this.validatorEmail}
                           required
                    />
                    
                    <input type="password"
                           className="Login__input"
                           placeholder="Пароль"
                           ref = "password"
                           onChange={this.handlePassword}
                           required
                    />
                    
                    <button onClick={this.enter}>Увійти</button>
                    <a href="#">Забули пароль?</a>
                    <div className="Login__footer">
                        <button>
                            <Link to="/reg">Зареєструватися</Link>
                        </button>
                    </div>
                </div>
                <center>
                    <div className="Login__modal__close" ref = "modal">
                        <div className="Login__modal--header">
                            <img onClick={this.closeModal} src="./../../assets/img/ico_close.svg" alt="X" width="15" height="15"/>
                        </div>
                        <div className="Login__modal--body">
                            <img src= "./../../assets/img/error.svg" alt="error" width="44" height="44"/>
                            <p>Невірний <br/> логін або пароль</p>
                        </div>
                        <div className="Login__modal--footer">
                            <p onClick={this.closeModal}> Спробувати ще раз </p>
                        </div>
                    </div>
                </center>
            </div>
        )
    }
}


function mapStateToProps(state) {
    return {
        login: state.login,
        auth: state.auth
    }
}

function mapDispatchToProps(dispatch) {
    return {
        AuthActions: bindActionCreators(AuthActions, dispatch)
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));