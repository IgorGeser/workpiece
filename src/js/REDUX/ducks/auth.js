/*Constants*/
const VALIDATE_NAME = 'VALIDATE_NAME';
const VALIDATE_EMAIL = 'VALIDATE_EMAIL';
const VALIDATE_PHONE = 'VALIDATE_PHONE';
const MODAL_VIEW = 'MODAL_VIEW';
const VALIDATE_PASSWORD = 'VALIDATE_PASSWORD';
const CHECK_PASSWORD = 'CHECK_PASSWORD';
const IS_VALIDATE = 'IS_VALIDATE';
const REMOVE_VALIDATION = 'REMOVE_VALIDATION';
const REQUEST_LOGIN = 'REQUEST_LOGIN';
const SUCCESS_LOGIN = 'SUCCESS_LOGIN';
const FAILURE_LOGIN = 'FAILURE_LOGIN';
const REQUEST_REGISTRATION = 'REQUEST_REGISTRATION';
const SUCCESS_REGISTRATION = 'SUCCESS_REGISTRATION';
const FAILURE_REGISTRATION = 'FAILURE_REGISTRATION';

/*Actions*/

export function validate_name(props) {
    return {
        type: VALIDATE_NAME,
        payload: props,
    }
}

export function validate_email(props) {
    return {
        type: VALIDATE_EMAIL,
        payload: props,
    }
}

export function validate_phone(props) {
    return {
        type: VALIDATE_PHONE,
        payload: props,
    }
}

export function validate_password(props) {
    return {
        type: VALIDATE_PASSWORD,
        payload: props,
    }
}

export function check_password(props) {
    return {
        type: CHECK_PASSWORD,
        payload: props,
    }
}

export function is_validate(props) {
    return {
        type: IS_VALIDATE,
        payload: props,
    }
}

export function remove_validation(props) {
    return {
        type: REMOVE_VALIDATION,
        payload: props,
    }
}

export function request_login(props) {
    return {
        type: REQUEST_LOGIN,
        payload: props,
    }
}

export function success_login(props) {
    return {
        type: SUCCESS_LOGIN,
        payload: props,
    }
}

export function failure_login(props) {
    return {
        type: FAILURE_LOGIN,
        payload: props,
    }
}

export function request_registration(props) {
    return {
        type: REQUEST_REGISTRATION,
        payload: props,
    }
}

export function success_registration(props) {
    return {
        type: SUCCESS_REGISTRATION,
        payload: props,
    }
}

export function failure_registration(props) {
    return {
        type: FAILURE_REGISTRATION,
        payload: props,
    }
}

export function modal_view(props) {
    return {
        type: MODAL_VIEW,
        payload: props
    }
}

/*Initial State*/
const initialState = {
    validateName: 'empty',
    validateEmail: 'empty',
    validateTelephone: 'empty',
    validatePassword: 'empty',
    checkPassword: 'empty',
    isValidate: false,
    view: false
};

/*Reducer*/
export default function reducer(state = initialState, action) {
    console.log(action);

    switch (action.type) {
        case VALIDATE_NAME:
            return {...state, validateName: action.payload};

        case VALIDATE_EMAIL:
            return {...state, validateEmail: action.payload};

        case VALIDATE_PHONE:
            return {...state, validateTelephone: action.payload};

        case VALIDATE_PASSWORD:
            return {...state, validatePassword: action.payload};

        case CHECK_PASSWORD:
            return {...state, checkPassword: action.payload};

        case MODAL_VIEW:
            return {...state, view: action.payload};

        case IS_VALIDATE:
            return {...state, isValidate: true};

        case REMOVE_VALIDATION:
            return {...state, isValidate: false};

        case REQUEST_LOGIN:
            return {...state,};

        case SUCCESS_LOGIN:
            return {...state,};

        case FAILURE_LOGIN:
            return {...state,};

        case REQUEST_REGISTRATION:
            return {...state,};

        case SUCCESS_REGISTRATION:
            return {...state,};

        case FAILURE_REGISTRATION:
            return {...state,};


        default:
            return state;
    }
}