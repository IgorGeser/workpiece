import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import 'leaflet/dist/leaflet-src';
import 'leaflet/dist/leaflet.css';
import  { basemapLayer } from 'esri-leaflet'


class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
        	map: '',
			text: 'default',
			marker: '',
			coord: new Array,
			all: new Array,
			L: ''
        }

        this.click = this.click.bind(this)
		this.testlol = this.testlol.bind(this)
    }

	componentDidMount() {
		let Lmap = L.map('map', {
			zoomControl: false,
			minZoom: 3,
			renderer: L.canvas(),
			attributionControl: false
		}).setView([49.8, 24], 7);

		this.setState({
			map: Lmap,
			L: L
		})
		let layer = basemapLayer('Topographic');
		Lmap.addLayer(layer);

		let atribution = L.control.attribution({
			prefix: '<a href="opendata.ua">opendata.ua</a>'
		});

		Lmap.addControl(atribution)

		Lmap.on('click', this.click)


    }

	testlol(e) {

    	this.setState({
			text: 'anything text'
		})

		let buff = this.state.coord.filter((item, index) => {
			if (item.lat == e.latlng.lat && item.lng == e.latlng.lng)
				return item
		})
		console.log(buff)
		let newMarker = L.popup()
			.setLatLng(e.latlng)
			.setContent(buff[0] + ' ')
			.openOn(this.state.map)

        this.state.all.all[length].icon.bindTooltip('olollol', {permanent: true, className: "my-label", offset: [0, 0] });

    }

    click(e) {

		let info = {}
		info.latlng = e.latlng
		info.icon



		let iconka = L.icon({
            iconUrl: './../../assets/img/ico_close.svg',

            iconSize:     [38, 95], // size of the icon
		})

		let marker = {
        	all: []
		}

        info.icon = L.marker(info.latlng, {icon: iconka})
        marker.all.push(info)
        marker.all[length].icon.bindTooltip(this.state.text, {permanent: true, className: "my-label", offset: [0, 0] });
        marker.all[length].icon.addTo(this.state.map);
        marker.all[length].icon.on('click', this.testlol)

		let buff = this.state.coord
		buff.push(e.latlng)
		this.setState({
			coord: buff,
			all: marker
		})
		// marker.bindPopup("<h5> lol </h5>").openPopup()
    }

	componentDidUpdate() {
		window.scrollTo(0, 0);
	}

	render() {
		return (
			<div className="App">
				<div className="App-header">
					asd
				</div>
				<div className="main" id="map" ref="map"/>
			</div>

		);
	}
}

export default withRouter(connect()(App));
