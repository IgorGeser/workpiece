new Vue({
    el: "#app",
    data: {
        currentView: 'square'
    },
    components: {
        square: {
            template: '<div class="square"></div>'
        },
        triangle: {
            template: '#triangle-template'
        },
        circle: {
            template: '#circle-template'
        },
        egg: {
            template: '#egg-template'
        }
    },
    methods: {
        switchView: function(view) {
            this.currentView = view
        }
    }
})


        
