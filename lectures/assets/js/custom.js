/*=============================================================
    Authour URL: www.designbootstrap.com
    
    http://www.designbootstrap.com/

    License: MIT

    http://opensource.org/licenses/MIT

    100% Free To use For Personal And Commercial Use.

    IN EXCHANGE JUST TELL PEOPLE ABOUT THIS WEBSITE
   
========================================================  */

$(document).ready(function() {

	/*====================================
	SCROLLING SCRIPTS
	======================================*/

	$('.scroll-me a').bind('click', function (event) { //just pass scroll-me in design and start scrolling
	var $anchor = $(this);
	$('html, body').stop().animate({
	scrollTop: $($anchor.attr('href')).offset().top
	}, 1200, 'easeInOutExpo');
	event.preventDefault();
	});


	/*====================================
	SLIDER SCRIPTS
	======================================*/


	$('#carousel-slider').carousel({
		interval: 4000 //TIME IN MILLI SECONDS
	});


	/*====================================
	VAGAS SLIDESHOW SCRIPTS
	======================================*/
	// $.vegas('slideshow', {
	// 	backgrounds: [{
	// 		src: 'assets/img/1.jpg',
	// 		fade: 1000,
	// 		delay: 9000
	// 	}, {
	// 		src: 'assets/img/2.jpg',
	// 		fade: 1000,
	// 		delay: 9000
	// 	}, ]
	// })


	/*====================================
	POPUP IMAGE SCRIPTS
	======================================*/
	$('.fancybox-media').fancybox({
	openEffect: 'elastic',
	closeEffect: 'elastic',
	helpers: {
	title: {
	type: 'inside'
	}
	}
	});


	/*====================================
	FILTER FUNCTIONALITY SCRIPTS
	======================================*/



	$(window).load(function() {
		var $container = $('#work-div');
		$container.isotope({
			filter: '*',
			animationOptions: {
				duration: 750,
				easing: 'linear',
				queue: false
			}
		});
		$('.caegories a').click(function() {
			$('.caegories .active').removeClass('active');
			$(this).addClass('active');
			var selector = $(this).attr('data-filter');
			$container.isotope({
				filter: selector,
				animationOptions: {
					duration: 750,
					easing: 'linear',
					queue: false
				}
			});
			return false;
		});

	});


	/*====================================
	WRITE YOUR CUSTOM SCRIPTS BELOW
	======================================*/

	$(window).scroll(function(){
		nav = $('.navbar')
		buff = $(window).scrollTop()

		if(buff > 20){
			$(nav).css("background-color","black");
        	$(nav).css("transition",".4s");
            // $(nav).css("height","66px");
		}
		else{
			$(nav).css("transition",".4s")
        	$(nav).css("background-color","transparent");
            // $(nav).css("height","97px");
		}
	})


	$('#parallax').parallax({imageSrc: 'assets/img/02.jpg'});



});