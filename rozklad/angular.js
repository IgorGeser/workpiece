var kvrozklad = angular.module('kvrozklad', ['ngRoute']);

kvrozklad.controller('FirstCtrl', function($scope, $http) {
    $scope.info = $http.get('https://api.rozklad.hub.kpi.ua/groups/580/timetable/').success(function(data) {
        $scope.json = data.data;


        var week = []
        $scope.Monday = {};
        $scope.Tuesday = {};
        $scope.Wednesday = {};
        $scope.Thursday = {};
        $scope.Friday = {};
        $scope.Saturday = {};
        week.push($scope.Monday, $scope.Tuesday, $scope.Wednesday, $scope.Thursday, $scope.Friday, $scope.Saturday)
        for (var i = 0; i < 6; i++) {
            week[i].First = {};
            week[i].Second = {};
            week[i].Third = {};
            week[i].Fourth = {};
            week[i].Fifth = {};
        }

        for (var j = 1; j <= 6; j++) //Day
        {
            for (var i = 1; i <= 6; i++) { //Lessons

                try {

                    // console.log("firsttry");
                    // console.log(i);
                    lesson = ($scope.json[1][j][i].discipline.name);

                    try {
                        teacher = ($scope.json[1][j][i].teachers[0].short_name);
                        room = ($scope.json[1][j][i].rooms[0].name);
                        build = ($scope.json[1][j][i].rooms[0].building.name);
                        build = '-' + build;
                        type = ($scope.json[1][j][i].type);

                        
                    } catch (e) {
                        teacher = '';
                        room = '';
                        build = '';
                    }
                    switch (i) {
                        case 1:
                            if (lesson == 'Іноземна мова-2. Іноземна мова гуманітарного спрямування') {
                                lesson = 'ANGL YAZ';
                            }
                            if(type == 0)
                        	type = 'Лекция';
                        	if(type == 1)
                        	type = 'Практика';
                        	if(type == 2)
                        	type = 'Лабораторная';
                       		 if(type == null)
                        	type = '';
                        	if(lesson == 'Фізичне виховання.')
                        	type = '';
                            week[j - 1].First.lesson = lesson;
                            week[j - 1].First.teacher = teacher;
                            week[j - 1].First.room = room;
                            week[j - 1].First.build =  build;
                            week[j - 1].First.day = day;
                            week[j - 1].First.type = type;
                            break;
                        case 2:
                            if (lesson == 'Іноземна мова-2. Іноземна мова гуманітарного спрямування') {
                                lesson = 'ANGL YAZ';
                            }
                            if(type == 0)
                        	type = 'Лекция';
                        if(type == 1)
                        	type = 'Практика';
                        if(type == 2)
                        	type = 'Лабораторная';
                        if(type == null)
                        	type = '';
                        if(lesson == 'Фізичне виховання.')
                        	type = '';
                            week[j - 1].Second.lesson = lesson;
                            week[j - 1].Second.teacher = teacher;
                            week[j - 1].Second.room = room;
                            week[j - 1].Second.build = build;
                            week[j - 1].Second.day = day;
                            week[j - 1].Second.type = type;
                            break;
                        case 3:
                            if (lesson == 'Іноземна мова-2. Іноземна мова гуманітарного спрямування') {
                                lesson = 'ANGL YAZ';
                            }
                            if(type == 0)
                        	type = 'Лекция';
                        if(type == 1)
                        	type = 'Практика';
                        if(type == 2)
                        	type = 'Лабораторная';
                        if(type == null)
                        	type = '';
                        if(lesson == 'Фізичне виховання.')
                        	type = '';
                            week[j - 1].Third.lesson = lesson;
                            week[j - 1].Third.teacher = teacher;
                            week[j - 1].Third.room = room;
                            week[j - 1].Third.build = build;
                            week[j - 1].Third.day = day;
                            week[j - 1].Third.type = type;
                            break;
                        case 4:
                            if (lesson == 'Іноземна мова-2. Іноземна мова гуманітарного спрямування') {
                                lesson = 'ANGL YAZ';
                            }
                            if(type == 0)
                        	type = 'Лекция';
                        if(type == 1)
                        	type = 'Практика';
                        if(type == 2)
                        	type = 'Лабораторная';
                        if(type == null)
                        	type = '';
                        if(lesson == 'Фізичне виховання.')
                        	type = '';
                            week[j - 1].Fourth.lesson = lesson;
                            week[j - 1].Fourth.teacher = teacher;
                            week[j - 1].Fourth.room = room;
                            week[j - 1].Fourth.build = build;
                            week[j - 1].Fourth.day = day;
                            week[j - 1].Fourth.type = type;
                            break;
                        case 5:
                            if (lesson == 'Іноземна мова-2. Іноземна мова гуманітарного спрямування') {
                                lesson = 'ANGL YAZ';
                            }
                            if(type == 0)
                        	type = 'Лекция';
                        if(type == 1)
                        	type = 'Практика';
                        if(type == 2)
                        	type = 'Лабораторная';
                        if(type == null)
                        	type = '';
                        if(lesson == 'Фізичне виховання.')
                        	type = '';
                            week[j - 1].Fifth.lesson = lesson;
                            week[j - 1].Fifth.teacher = teacher;
                            week[j - 1].Fifth.room = room;
                            week[j - 1].Fifth.build = build;
                            week[j - 1].Fifth.day = day;
                            week[j - 1].Fifth.type = type;

                            break;

                    }

                } catch (e) {

                    switch (i) {
                        case 1:
                            week[j - 1].First.lesson = '';
                            break;
                        case 2:
                            week[j - 1].Second.lesson = '';
                            break;
                        case 3:
                            week[j - 1].Third.lesson = '';
                            break;
                        case 4:
                            week[j - 1].Fourth.lesson = '';

                            break;
                        case 5:
                            week[j - 1].Fifth.lesson = '';
                            break;
                    }
                }

            }
        }
        $scope.listArray = new Array;
        for (var i = 0; i < week.length; i++) {
            switch (i) {
                case 0:
                    week[0] = $scope.Monday;
                    break;
                case 1:
                    week[1] = $scope.Tuesday;
                    break;
                case 2:
                    week[2] = $scope.Wednesday;
                    break;
                case 3:
                    week[3] = $scope.Thursday;
                    break;
                case 4:
                    week[4] = $scope.Friday;
                    break;
                case 5:
                    week[5] = $scope.Saturday;
                    break;
            }
        }
    });

    // dateNow = new Date();
    // nowDay = dateNow.getDate();
    // nowMonth = dateNow.getMonth();
    // dateBegin = new Date(2017, 1, 13);
    // test = (new Date(2017, nowMonth, nowDay));
    // aboutWeek = Math.trunc((dateBegin - test) / 7) % 2 == 0 ? 0 : 1;
    // var time = new Date();
    // var day = time.getDay();
    // if (aboutWeek == 1) {
    //     if (day == 0)
    //         day = 1;
    // } else
    //     day = 'null';
    // console.log(aboutWeek);
var day = new Date().getDay();
var year = new Date().getFullYear();
var month = new Date().getMonth();
var today = new Date(year, month, 0).getTime();
var now = new Date().getTime();
var week = Math.round((now - today) / (1000 * 60 * 60 * 24 * 7));
if (week % 2) {
    console.log("Вторая неделя");
    day = 'null';
} else {
    console.log("Первая неделя");
    if(day == 0) day = 1;
}
    $scope.time = day;
});



kvrozklad.controller('SecondCtrl', function($scope, $http) {
    $scope.info = $http.get('https://api.rozklad.hub.kpi.ua/groups/580/timetable/').success(function(data) {
        $scope.json = data.data;


        var week = []
        $scope.Monday = {};
        $scope.Tuesday = {};
        $scope.Wednesday = {};
        $scope.Thursday = {};
        $scope.Friday = {};
        $scope.Saturday = {};
        week.push($scope.Monday, $scope.Tuesday, $scope.Wednesday, $scope.Thursday, $scope.Friday, $scope.Saturday)
        for (var i = 0; i < 6; i++) {
            week[i].First = {};
            week[i].Second = {};
            week[i].Third = {};
            week[i].Fourth = {};
            week[i].Fifth = {};
        }

        for (var j = 1; j <= 6; j++) //Day
        {
            for (var i = 1; i <= 6; i++) { //Lessons

                try {

                    // console.log("firsttry");
                    // console.log(i);
                    lesson = ($scope.json[2][j][i].discipline.name);

                    try {
                        teacher = ($scope.json[2][j][i].teachers[0].short_name);
                        room = ($scope.json[2][j][i].rooms[0].name);
                        build = ($scope.json[2][j][i].rooms[0].building.name)
                        build = '-' + build;
                        type = ($scope.json[2][j][i].type);		

                    } catch (e) {
                        teacher = '';
                        room = '';
                        build = '';
                    }
                      // build = '-' + build;
                    switch (i) {
                        case 1:
                            if (lesson == 'Іноземна мова-2. Іноземна мова гуманітарного спрямування') {
                                lesson = 'ANGL YAZ';
                            }
                            if(type == 0)
                        	type = 'Лекция';
                        if(type == 1)
                        	type = 'Практика';
                        if(type == 2)
                        	type = 'Лабораторная';
                        if(type == null)
                        	type = '';
                        if(lesson == 'Фізичне виховання.')
                        	type = '';
                            week[j - 1].First.lesson = lesson;
                            week[j - 1].First.teacher = teacher;
                            week[j - 1].First.room = room;
                            week[j - 1].First.build = build;
                            week[j - 1].First.day = day;
                            week[j - 1].First.type = type;
                            break;
                        case 2:
                            if (lesson == 'Іноземна мова-2. Іноземна мова гуманітарного спрямування') {
                                lesson = 'ANGL YAZ';
                            }
                            if(type == 0)
                        	type = 'Лекция';
                        if(type == 1)
                        	type = 'Практика';
                        if(type == 2)
                        	type = 'Лабораторная';
                        if(type == null)
                        	type = '';
                        if(lesson == 'Фізичне виховання.')
                        	type = '';
                            week[j - 1].Second.lesson = lesson;
                            week[j - 1].Second.teacher = teacher;
                            week[j - 1].Second.room = room;
                            week[j - 1].Second.build = build;
                            week[j - 1].Second.day = day;
                            week[j - 1].Second.type = type;
                            break;
                        case 3:
                            if (lesson == 'Іноземна мова-2. Іноземна мова гуманітарного спрямування') {
                                lesson = 'ANGL YAZ';
                            }
                            if(type == 0)
                        	type = 'Лекция';
                        if(type == 1)
                        	type = 'Практика';
                        if(type == 2)
                        	type = 'Лабораторная';
                        if(type == null)
                        	type = '';
                        if(lesson == 'Фізичне виховання.')
                        	type = '';
                            week[j - 1].Third.lesson = lesson;
                            week[j - 1].Third.teacher = teacher;
                            week[j - 1].Third.room = room;
                            week[j - 1].Third.build = build;
                            week[j - 1].Third.day = day;
                            week[j - 1].Third.type = type;
                            break;
                        case 4:
                            if (lesson == 'Іноземна мова-2. Іноземна мова гуманітарного спрямування') {
                                lesson = 'ANGL YAZ';
                            }
                            if(type == 0)
                        	type = 'Лекция';
                        if(type == 1)
                        	type = 'Практика';
                        if(type == 2)
                        	type = 'Лабораторная';
                        if(type == null)
                        	type = '';
                        if(lesson == 'Фізичне виховання.')
                        	type = '';
                            week[j - 1].Fourth.lesson = lesson;
                            week[j - 1].Fourth.teacher = teacher;
                            week[j - 1].Fourth.room = room;
                            week[j - 1].Fourth.build = build;
                            week[j - 1].Fourth.day = day;
                            week[j - 1].Fourth.type = type;
                            break;
                        case 5:
                            if (lesson == 'Іноземна мова-2. Іноземна мова гуманітарного спрямування') {
                                lesson = 'ANGL YAZ';
                            }
                            if(type == 0)
                        	type = 'Лекция';
                        	if(type == 1)
                        	type = 'Практика';
                        	if(type == 2)
                        	type = 'Лабораторная';
                       		if(type == null)
                        	type = '';
                        	if(lesson == 'Фізичне виховання.')
                        	type = '';
                            week[j - 1].Fifth.lesson = lesson;
                            week[j - 1].Fifth.teacher = teacher;
                            week[j - 1].Fifth.room = room;
                            week[j - 1].Fifth.build = build;
                            week[j - 1].Fifth.day = day;
                            week[j - 1].Fifth.type = type;
                            break;

                    }

                } catch (e) {

                    switch (i) {
                        case 1:
                            week[j - 1].First.lesson = '';
                            break;
                        case 2:
                            week[j - 1].Second.lesson = '';
                            break;
                        case 3:
                            week[j - 1].Third.lesson = '';
                            break;
                        case 4:
                            week[j - 1].Fourth.lesson = '';

                            break;
                        case 5:
                            week[j - 1].Fifth.lesson = '';
                            break;
                    }
                }

            }
        }
        for (var i = 0; i < week.length; i++) {
            switch (i) {
                case 0:
                    week[0] = $scope.Monday;
                    break;
                case 1:
                    week[1] = $scope.Tuesday;
                    break;
                case 2:
                    week[2] = $scope.Wednesday;
                    break;
                case 3:
                    week[3] = $scope.Thursday;
                    break;
                case 4:
                    week[4] = $scope.Friday;
                    break;
                case 5:
                    week[5] = $scope.Saturday;
                    break;
            }
        }
    });

    // dateNow = new Date();
    // nowDay = dateNow.getDate();
    // nowMonth = dateNow.getMonth();
    // dateBegin = new Date(2017, 1, 13);
    // test = (new Date(2017, nowMonth, nowDay));
    // dateBegin = new Date(2017, 1, 13);
    // aboutWeek = Math.trunc(new Date(dateBegin - test) / 7) % 2 == 0 ? 0 : 1;
    // var time = new Date();
    // var day = time.getDay();
    // if (aboutWeek == 1) {
    //     if (day == 0)
    //         day = 1;
    // } else {
    //     day = 'null';
    // }

    var day = new Date().getDay();
    var year = new Date().getFullYear();
    var month = new Date().getMonth();
    var today = new Date(year, month, 0).getTime();
    var now = new Date().getTime();
    var week = Math.round((now - today) / (1000 * 60 * 60 * 24 * 7));
    if (week % 2) {
        console.log("Вторая неделя");
        if(day == 0) day = 1;

    } else {
        console.log("Первая неделя");
          day = 'null';
    }
    $scope.time = day;
    // console.log(aboutWeek + 'second');
    // console.log(isFirstWeek + ' about week');
});




